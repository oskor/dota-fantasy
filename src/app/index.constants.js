/* global _:false, moment:false, firebase:false */
(function() {
  'use strict';

  angular
    .module('dotaFantasy')
    .constant('_', _)
    .constant('moment', moment)
    .constant('firebase', firebase);
})();
