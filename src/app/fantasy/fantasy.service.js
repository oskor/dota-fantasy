(function() {
    'use strict';

    angular
      .module('dotaFantasy')
      .service('fantasyService', fantasyService);

    function fantasyService(_, $q, fantasyRepo, $firebaseObject) {

    
        var _teams = [];
        // Initialize Firebase
    

        function loadLeagueTeams(league_id) {
            var teams = [];
            return $q.when(teams);
            // return fantasyRepo.getTeamInfo(league_id).then(function(teams) {
            //     return fantasyRepo.getProPlayerList().then(function(players) {
            //         _teams = _.map(teams, function(team) {
            //             team.created = new Date(team.time_created * 1000);
            //             _.map(team.members, function(m) {
            //                 var p = _.find(players, {account_id: m.account_id});
            //                 //if(p) {
            //                 //    p.fantasy = fantasyRepo.getTournamentPlayerStats(league_id, p.account_id).then(function(player) {
            //                 //        return _.map(player, decoratePlayerStats);
            //                 //    });
            //                 //}
            //                 return _.extend(m, p, {active: false, stats: { fantasy: {total: 0}}});
            //             });
            //             return team;
            //         });
            //         return _teams;
            //     });
                
            // });
        }

        function loadProPlayerList() {
            return fantasyRepo.getProPlayerList().then(function(players) {
                return _.map(player, decoratePlayerStats);
            });
        }

        function loadTournamentPlayerStats(league_id, account_id) {
            return fantasyRepo.getTournamentPlayerStats(league_id, account_id).then(function(stats) {
                return decoratePlayerStats(stats);
            });
        }

        function decoratePlayerStats(p) {
            console.log(p);
            var denies_average = _.sum(_(p.matches).map('denies').value()) / p.matches.length
            var averages = {
                kills: p.kills_average,
                assist: p.assists_average,
                deaths: p.deaths_average,
                gpm: p.gpm_average,
                last_hits: p.last_hits_average,
                denies: denies_average,
                networth: p.networth_average,
                xpm: p.xpm_average
            };
            var fantasy = calculateFantasyPoints(averages);
            fantasy.total = _.reduce(fantasy, function(result, v, k) {
                return result += v;
            });
            //fantasy.total = fantasy.total * 1.4;
            return {
                averages: averages,
                fantasy: fantasy
            };
        }

        function calculateFantasyPoints(stats) {
            return {
                kills: stats.kills * 0.3,
                deaths: 5 - (stats.deaths * 0.3),
                last_hits: (stats.last_hits + stats.denies) * 0.003,
                gpm: stats.gpm * 0.002
            };
        }

        return {
            loadLeagueTeams: loadLeagueTeams,
            loadTournamentPlayerStats: loadTournamentPlayerStats
        };
    }
})();