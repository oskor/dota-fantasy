(function() {
  'use strict';

  angular
      .module('dotaFantasy')
      .service('fantasyRepo', fantasyRepo);

	function fantasyRepo(simpleResource) {
    return {
      getTeamInfo: function(league_id) {
        return simpleResource.newResource().withKey().withParam('league_id',league_id).get('/IDOTA2Teams_570/GetTeamInfo/v1/').then(function(response) {
          return response.teams;
        });
      },
      getProPlayerList: function() {
        return simpleResource.newResource().withKey().get('/IDOTA2Fantasy_570/GetProPlayerList/v1/').then(function(response) {
          return response.player_infos;
        });
      },
      getTournamentPlayerStats: function(league_id, account_id) {
        return simpleResource.newResource().withKey().withParam('league_id',league_id).withParam('account_id',account_id).get('/IDOTA2Match_570/GetTournamentPlayerStats/v2/').then(function(response) {
          //console.log(response.result);
          return response.result;
        });
      }
    };
  }
})();