(function() {
  'use strict';

  angular
    .module('dotaFantasy')
    .controller('FantasyController', FantasyController);

  /** @ngInject */
  function FantasyController($scope, $filter, _, $firebaseObject, moment) {
    var vm = this;
    var _players = [];
    vm.league_id = "4664";
    vm.teams = [];
    vm.players = [];

    vm.sortOptions = [{name:'Name',id:'name'}, {name:'Team',id:'team_name'}, {name: "Kills", id: 'stats.fantasy.kills'}
    , {name:'Deaths',id:'stats.fantasy.deaths'}, {name:'Creep Score',id:'stats.fantasy.last_hits'}, {name:'GPM',id:'stats.fantasy.gpm'}
    , {name:'Fight%',id:'stats.fantasy.team_fight'}, {name:'Runes',id:'stats.fantasy.runes'}, {name:'FB',id:'stats.fantasy.first_blood'}, {name:'Wards',id:'stats.fantasy.wards'}
    , {name:'Towers',id:'stats.fantasy.towers'}, {name:'Total',id:'stats.fantasy.total'}];
    vm.sortBy = 11;
    vm.predicate = vm.sortOptions[vm.sortBy].id;
    vm.reverse = true;
    vm.order = function(index) {
      vm.reverse = (vm.sortBy === index) ? !vm.reverse : true;
      vm.sortBy = index;
      vm.predicate = vm.sortOptions[vm.sortBy].id;
    };
    vm.showUpdated = true;
    vm.filter = {};
    vm.filterOptions = [{name:'Role',filters:[{name: 'Core', data: {'fantasy_role': 1}},{name: 'Support', data: {'fantasy_role': 2}}]}];
    vm.filterPlayers = function(filter) {
      vm.filter = filter;
      vm.sets = _.filter(_players, filter);
    };

    vm.deadTeams = [
      {name: 'Team Secret', id: 1838315},
      {name: 'Vici_Gaming Reborn', id: 2777247},
      {name: 'Escape Gaming', id: 2783913},
      {name: 'Natus Vincere', id: 36},
      {name: 'OG Dota2', id: 2586976},
      {name: 'LGD-GAMING', id: 15},
      {name: 'compLexity Gaming', id: 3},
      {name: 'Execration', id: 2581813},
      {name: 'Alliance', id: 111474},
      {name: 'Newbee', id: 1375614},
      {name: 'TNC Pro Team', id: 2108395},
      {name: 'Team Liquid', id: 2163},
      {name: 'EHOME', id: 4},
      {name: 'MVP Phoenix', id: 1148284},
      {name: 'Fnatic', id: 350190}
    ];

    var config = {
      apiKey: "AIzaSyCCLz-StZzlNLN5-lhX7UrQQgrQff-sFfY",
      authDomain: "dota-fantasy.firebaseapp.com",
      databaseURL: "https://dota-fantasy.firebaseio.com"
    };
    firebase.initializeApp(config);

    firebase.database().ref('fantasy/members').limitToLast(1).once('value', function(players) {
      $scope.$apply(function() {
        //vm.teams = teams.val().teams;
        _players = players.val();
        console.log(_players);
        var lastUpdated = _.map(_players, function(v, k) {return k;})[0];
        console.log('lastUpdated', lastUpdated);
        vm.lastUpdated = moment(_.toNumber(lastUpdated)).fromNow();
        _players = _.map(_players,function(v) { return _.values(v);})[0];
        

        //_players = _.flatten(_.map(vm.teams, function(v, i) {return _.map(v.members, function(v,i){return v;});}));
        vm.players = _.filter(_players, function(v, k) {
          return !_.some(vm.deadTeams, {id: v.team_id});
        });
        //console.log(_players);
      });
    });
    
    

  }
})();
