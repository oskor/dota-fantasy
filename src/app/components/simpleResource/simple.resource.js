(function() {
    'use strict';

    angular
      .module('dotaFantasy')
      .service('simpleResource', simpleResource);

    function simpleResource($http, $q) {
        
        function newResource() {

            var _steamApi = "https://api.steampowered.com";
            var _resourceUrl = "";
            var _apiKey = "";
            var _params = [];
            
            function createUrl() {
                //don't append any extra parameters if I'm only requesting a local json file.
                if(_resourceUrl.indexOf('.json')>0) {
                    return _resourceUrl;
                }
                else {
                    return _resourceUrl + '?' + _apiKey + addParams();
                    //return _steamApi + _resourceUrl + '?' + _apiKey + addParams();
                }
            }
            function addParams() {
                return _params.map(function(param) {
                    return '&' + param.name + '=' + param.key;
                });
            }

            return {
                get: function(resourceUrl) {
                    var deferred = $q.defer();
                    _resourceUrl = resourceUrl;
                    var url = createUrl();
                    //console.log(url);
                    $http.get(url).success(function(data) {
                        return deferred.resolve(data);
                    });
                    return deferred.promise;
                },
                withKey: function() {
                    _apiKey = 'key=FBCD0235AE226119D146C8728F30AAC8';
                    return this;
                },
                withParam: function(i, k) {
                    _params.push({name: i, key: k});
                    return this;
                }
            };
        }

        return {
            newResource: newResource
        };
    }
})();
