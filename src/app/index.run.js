(function() {
  'use strict';

  angular
    .module('dotaFantasy')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
