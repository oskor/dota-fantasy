(function() {
  'use strict';

  angular
    .module('dotaFantasy')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/home',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('fantasy', {
        url: '/',
        templateUrl: 'app/fantasy/fantasy.html',
        controller: 'FantasyController',
        controllerAs: 'ft'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
