(function() {
  'use strict';

  angular
    .module('dotaFantasy', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ui.router', 'toastr', 'firebase']);

})();
